# Projekt om text spel

Målet med detta projekt är att du ska skapa ett enkelt textbaserat spel, med
vårt ramverk som för närvarande har det ointuitiva namnet "mimer-game-server".

För att göra detta går du till vår playground för textbaserade spel som du
hittar [här](https://htsit.se/x/playground/). Där finns det exempel på spel samt
en lista på förbättringsförslag och buggar som vi förvärvade arbetar med att
lösa. Du kan göra ändringar i exemplena eller börja med ett helt tomt spel. Men
börja med att läsa exemplena för att se hur de är skrivna.

Om du inte kommer på några idéer har vi några förslag
[här](https://htsit.se/x/playground/förslag.html).

# Spelens konstruktion

Du har dels ett objekt som du kan ändra data i som används för att konfigurera
spelet och en klass där du kan skriva ditt program i.

En klass är ett sätt att skapa objekt med. En klass kan du tänka på som en mall
för ett objekt. En klass består av medlemmar som blir egenskaper på det objekt
som skapas. Likaså metoder som är funktioner som hanterar klassen.

Medlemmarna och metoderna kan du i klassen komma åt genom att skriva _this_
följt av namnet på medlemmen, se exemplena.

En klass kan ärva från andra klasser och den klass vi implementerar för att få
ett spelen här har ärvt från _Game_ klassen. Detta ger vissa medlemmar och
metoder en särskilt innebörd.

## Medlemmar

Direkt under klassen har du medlemar som är variabler som hör ihop med klassen.
Dessa kan sättas utifrån objektet som används för konfiguration. Det är upp till
utvecklaren av varje spel att använda dessa medlemmar på bästa sätt i sitt spel.

### type

Bestämmer vilken "mode" spelet är i. Se rubriken nedan för vilka möjliga värden
som finns.

### title

Titeln på spelet. Kommer visas när spelet startas.

### label

En kort beskrivning av spelet som kommer visas när någon kan välja spelet i en
lista. Bör därför vara unikt för varje kofiguration av spelet.

### name

Namnet på spelet. Detta används intern av ramverket och måste vara unikt för
varje configuration av spelet.

## Methoder

### constructor

Denna metod anropas en gång när ett objekt skapas från klassen.

### start

När spelet startas anropas denna metod. Alla spelarna finns då i medlemmen
_players_.

### next

Den metod som du kommer använda mest. Den har två inparametrar, en för spelaren
och ett svar som spelaren har givit. Du skickar ett meddelande genom att sätta
egenskapen _msg_ på "player" till detta värde.

Parametern “answer” är ett objekt vars egenskaper är svar på frågor som du
ställt till användaren. Du gör detta genom att skriva en text med en ny rad som
börjar med “?” följt av namnet på egenskapen som ska spara svaret på frågan, du
kan sedan skriva själva texten i frågan. Se exemplerna för hur detta ser ut.

### Return värde

Methoden _next_ kan returna olika texter som tolkas av ramverket på olika sätt.

- _next_ Spelet ska forsätta.
- _end_ Spelet avslutas neutralt för spelaren.
- _win_ Spelaren har vunnit spelet.
- _lose_ Spelaren har förlorat spelet.

# Modes

## Singel

Används för en ensam spelare. “Next” anropas efter varandra.

## Turn

Använd när två eller flera spelare turas om att genomföra sina drag i en omgång.

## Concurent

Använd när två eller flera spelare kan genomföra sina drag samtidigt i en
omgång.

# Bedömning

Projektet kommer antingen ge omdömet F eller vara godkänd, observera dock att
ett bra projekt kan påverka betygsättning på positivt när detta görs i slutet av
kursen.

Försökt i övrigt vara så kreativ och använda den praxis för god programmering
som vi diskuterat.
