#!/usr/bin/env sh

echo "Copy playground to htsit.se/x"
scp -r editor krm@htsit.se:~/www/x/playground/
scp -r examples krm@htsit.se:~/www/x/playground/
ghmd index.md
scp -r index.html krm@htsit.se:~/www/x/playground/
ghmd uppgift.md
scp -r uppgift.html krm@htsit.se:~/www/x/playground/
ghmd förslag.md
scp -r förslag.html krm@htsit.se:~/www/x/playground/
echo "Finish!"
