# Mimer text game server

Här finns flera exempel på text spel som går att köra via vår server.

# Skapa ett eget spel

Du kan utgå från denna editor utan innehål.

- [Editor för att skapa spel](./editor/)

# Singel spel

- Test med olika texter [Editor](./editor/index.html?example=testOfText)
  [Spela](./editor/view.html?example=testOfText)
- Gissa talet [Editor](./editor/index.html?example=guessNumber)
  [Spela](./editor/view.html?example=guessNumber)
- Binära till hexadecimala tal [Editor](./editor/index.html?example=binHex)
  [Spela](./editor/view.html?example=binHex)
- Sagospelet Korsvägen [Editor](./editor/index.html?example=crossroads)
  [Spela](./editor/view.html?example=crossroads)

# I tur ordning

- 🚧 [Nimb]()

# Samtidigt

- 🚧 [Sten, sax, påse]()

# Att göra

## Buggar

- ✔️ All text skrivs inte ut.
- ✔️ När editorn anropas som tom visas ett error.

## Snarast

- ✔️ Fixa ett till exempel.
- ✔️ Fixa så exempel spel kan läsas in automatiskt.
- ✔️ Updatera configureringen så det blir klart vad varje egenskap ska göra.
- ✔️ Gör så titeln visas när spelet startas.
- ✔️ Gör så namnet på skaparen av spelet visas när spelet startas.
- ✔️ Skapa fler return värden för next som avslutar spelet på olika sätt.
- ✔️ Se till att dessa return värden har olika effekter när spelet avslutas.
- ✔️ Skapa en version av playground som bara visar spelet.
- ✔️ Gör så consolen blir mindre eller ta bort den helt.
- Fixa UI så ser snygare ut och går lättare att spela.
- Gör speltypen "turn".
- Konvertera exempelt Nimb till playground.

## Gör klart API:et

- Gör en seedad slump funktion i Game och använd den som standard.
- Gör ett simpel AI för att testa singel spel.
- Gör AI även för andra typer av spel.
- Gör speltypen "concurent".
- Skapa spelet "sten, sax, påse" för playground.

## Få igång game server

- Få game servern att fungera igen för singel spel.
- Få den att köras från htsit.se/x
- Undersök om det är möjligt att linka ts filer.
- Gör så att playground använder källkod filerna som i skrivna i ts direkt.
- Kombinera de två olika sättet att spara exempel spel till ett.
- Låt editorn kunna exportera spelet som en standalone variant av view.

## Skapa inlog och möjligheten att spela mot flera personer