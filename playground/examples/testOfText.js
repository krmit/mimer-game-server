// Game config

const configGame = {
  name: "TestOfText",
  max: 3,
  min: 1,
  label: "testar några olika texter",
  title: "Test av Text",
};

// Game code

class TestOfText extends Game {
  static creators = ["krm"];
  static description = "Ett test av olika saker";

  numberToGuess = 0;
  max;
  page = 1;
  texts = [];

  constructor(config) {
    super(config);
    this.page = config.min ?? 1;
    this.max = config.max ?? 8;

    let text = "";
    text = "# The quick brown fox jumps over the lazy dog";
    this.texts.push(text);

    text = "?answer Ge mig ett ord?";
    this.texts.push(text);

    text = "**The quick brown fox jumps over the lazy dog**";
    this.texts.push(text);

    text = "🔥 The quick brown fox jumps over the lazy dog";
    this.texts.push(text);

    text = "```The quick brown fox jumps over the lazy dog```";
    this.texts.push(text);

    text = "Abba\nBccb\nCddc\nDeed\nEffe";
    this.texts.push(text);

    text = "- Abba\n- Bccb\n- Cddc\n- Deed\n- Effe";
    this.texts.push(text);

    text = "1. Abba\n2. Bccb\n3. Cddc\n4. Deed\n5. Effe";
    this.texts.push(text);

    text = "Abba\nBccb\n🔥 Cddc\nDeed\nEffe";
    this.texts.push(text);
  }

  start() {
    let result = "Visar några test fram till test nummer " + this.max;
    return result;
  }

  next(player, answer) {
    player.msg += `\n# Test ${this.page}\n`;
    player.msg += this.texts[this.page - 1] + "\n";
    this.page++;
    console.log(answer);
    if (!(this.page < this.max + 2)) {
      return "end";
    }
    return "next";
  }
}

// Game start

window.onload = async function () {
  standalone(TestOfText, configGame);
};
