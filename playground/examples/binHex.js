// Game config

const configGame = {
  name: "binhex3",
  length: 3,
  title: "Binära till hexadecimala tal",
};

// Game code

class BinHex extends Game {
  static creators = ["krm"];
  static description;
  answer = "";
  letters;
  points = 0;
  turns = 0;
  length;

  constructor(config) {
    super(config);
    this.letters = config.max ?? 1;
    this.length = config.length ?? 5;
  }

  start() {
    let result = "";
    result += "Skriv vad det binära tal blir i hexadecimalt.\n";
    result += "Svara med små bokstäver\n";
    result += this.doQuestion(result);
    this.points = 0;
    this.turns = 0;
    return result;
  }

  next(player, answer) {
    if (answer.answer !== this.answer) {
      player.msg += `🙁 ${answer.answer} != ${this.answer}\n`;
    } else {
      player.msg += "🙂";
      this.points++;
    }
    this.turns++;
    if (this.turns < this.length) {
      player.msg += this.doQuestion(player.msg);
    } else {
      player.msg += "\nDu hade ";
      player.msg += String(this.points);
      player.msg += " rätt av " + this.length + ".";
      if (this.points / this.length > 0.9) {
        return "win";
      } else if (this.points / this.length > 0.5) {
        return "end";
      } else {
        return "lose";
      }
    }
    return "next";
  }

  doQuestion(result) {
    const solution = Math.floor(Math.random() * 16 ** this.letters);
    let binary_text = printBinary(solution, this.letters);
    this.answer = printHex(solution, this.letters);
    return "\n?answer " + binary_text + "?";
  }
}

function printBinary(bin, nibbles) {
  let filter = 1;
  let result = "";
  while (bin > filter - 1) {
    if ((bin & filter) === 0) {
      result = "0" + result;
    } else {
      result = "1" + result;
    }
    filter = filter << 1;
  }
  result = "0".repeat(nibbles * 4 - result.length) + result;

  return result;
}

function printHex(hex, nibbles) {
  let result = "";
  const alphabet = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
  ];

  for (let i = 0; i < nibbles; i++) {
    let nibbel = Math.floor((hex / 16 ** i) % 16);
    result = alphabet[nibbel] + result;
  }

  return result;
}

// Game start

window.onload = async function () {
  standalone(BinHex, configGame);
};
