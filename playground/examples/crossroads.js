// Game config

const configGame = {
  name: "Crossroads",
  title: "Korsvägen - En interaktiv saga",
};

// Game code

class Crossroads extends Game {
  static creators = ["foj"];
  static description = "Ett spel där du ska hitta en skatt.";
  type = "singel";
  state = "start";

  constructor(config) {
    super(config);
  }

  start() {
    let result = "";
    result += `Välkommen!\n\n Du befinner dig i en mystisk värld.\n Ditt uppdrag är att hitta en förlorad skatt. Du står vid en korsväg.\n`;
    result += "?choice Vilket håll väljer du? (vänster / höger): ";
    return result;
  }

  next(player, answer) {
    switch (this.state) {
      case "start":
        if (answer.choice === "vänster") {
          this.state = "vänster";
          player.msg += "Du har valt att gå vänster. Du kommer till en flod.\n";
          player.msg += "?choice Vad gör du? (simma / leta)";
        } else if (answer.choice === "höger") {
          this.state = "höger";
          player.msg +=
            "Du har valt att gå höger.\nDu kommer till en mörk skog.\n";
          player.msg +=
            "?choice Utforska skogen eller vänd tillbaka? (utforska / vänd)";
        } else {
          player.msg += "Ogiltigt val! Spelet är över.";
          return "end";
        }
        break;
      case "vänster":
        if (answer.choice === "simma") {
          player.msg +=
            "Du försöker simma över floden.\n Tyvärr, du dras med av strömmen och drunknar. Spelet är över!";
          return "end";
        } else if (answer.choice === "leta") {
          player.msg +=
            "Du hittar en gammal bro och korsar floden. Bra jobbat!\n";
          return "end";
        } else {
          player.msg += "Ogiltigt val! Spelet är över.";
          return "end";
        }
        break;
      case "höger":
        if (answer.choice === "utforska") {
          player.msg +=
            "Du går djupare in i skogen och hittar en gömd skatt!\n Grattis, du vann spelet!";
          return "end";
        } else if (answer.choice === "vänd") {
          this.state = "leta";
          player.msg += "Du vänder tillbaka. Spelet är över!\n";
          return "end";
        } else {
          player.msg += "Ogiltigt val! Spelet är över.";
          return "end";
        }
        break;
    }
    return "next";
  }
}

// Game start

window.onload = async function () {
  standalone(Crossroads, configGame);
};
