async function standalone(gameClass, gameConfig) {
  let player = new Player("Spelare", 0);
  let game = new gameClass(gameConfig);
  const title_node = document.getElementById("title");
  title_node.innerHTML = game.title;
  const creator_node = document.getElementById("creator");
  console.log(creator_node);
  console.log(gameClass.creators.join(","));
  creator_node.innerHTML = gameClass.creators.join(",");

  let msg = game.start();
  let status = "start";

  while (status !== "end" && status !== "win" && status !== "lose") {
    let result = await send(msg);
    status = game.next(player, result);
    msg = player.msg;
    player.msg = "";
  }

  msg += "\n";
  if (status === "win") {
    msg = "🎉🎉🎉 Grattis du vann!";
  } else if (status === "lose") {
    msg = "💀💀💀 Du förlorade!";
  }
  send(msg + "\nSpelet är slut!", true);
}

let terminal = "";

async function send(msg, end = false) {
  console.log(msg);
  const data = parse(msg);
  terminal += data.msg;
  const node = document.getElementById("terminal").childNodes;
  const input_div = node[3].childNodes;
  node[1].innerHTML = terminal;
  input_div[2].value = "";
  node[3].style.visibility = "hidden";
  node[5].style.visibility = "hidden";
  let event;
  if (end) {
    console.log("End game");
  } else if (data.question === "") {
    node[5].style.visibility = "visible";
    event = await new Promise((resolve, reject) => {
      node[5].addEventListener("click", resolve);
    });
    node[5].style.visibility = "hidden";
    console.log("> ok");
  } else {
    node[3].style.visibility = "visible";
    input_div[1].innerHTML = data.prompt;
    event = await new Promise((resolve, reject) => {
      input_div[2].addEventListener("change", resolve);
    });
    let input = event.target.value;
    terminal += data.prompt + " " + input + "\n";
    node[3].style.visibility = "hidden";
    return { [data.question]: input };
  }
  return {};
}

function parse(msg) {
  let html_msg = "";
  let lines = msg.trim().split("\n");
  let last_line = lines[lines.length - 1];

  let question = "";
  let prompt = "";

  if (last_line[0] === "?") {
    let data = last_line.split(" ");
    question = data[0].substring(1);
    prompt = data.slice(1).join(" ");
    lines.pop();
  }

  for (const line of lines) {
    if (line[0] === "#") {
      html_msg += "<h3>" + line.substring(1).trim() + "</h3>";
    } else {
      html_msg += line.trim() + "\n";
    }
  }

  result = {
    msg: html_msg,
    question: question,
    prompt: prompt,
  };
  return result;
}
