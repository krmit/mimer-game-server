class Player {
  constructor(name, id) {
    this.name = name;
    this.msg = "";
    this.id = id;
  }
}

class Game {
  constructor(config) {
    var _a, _b, _c;
    this.players = [];
    this.status = "start";
    this.type = "singel";
    this.msg = "";
    this.name = (_a = config.name) !== null && _a !== void 0 ? _a : "";
    this.title = (_b = config.title) !== null && _b !== void 0 ? _b : this.name;
    this.description =
      (_c = config.description) !== null && _c !== void 0 ? _c : this.name;
  }
  join(...players) {
    this.players = this.players.concat(players);
    for (let i = 0; i < players.length; i++) {
      players[i].id = this.players.length + i;
    }
  }
  get nextId() {
    return this.players.length + 1;
  }
}
