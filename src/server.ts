#!/usr/bin/node
import express from "express";
import cookieParser from "cookie-parser";
import { program } from "commander";
import { Player } from "./player";
import { Game, GameConfig, GameOptions } from "./game";
import { User } from "./user";

interface GameServerConfig {
  port?: number;
  simpel?: boolean;
  name?: string;
  title?: string;
  version?: string;
}

interface GameConstructor {
  new (config: GameConfig): Game;
}

interface GameAndOptions {
  game: GameConstructor;
  options: GameOptions;
}

export async function server(
  config: GameServerConfig,
  ...gameData: (typeof Game | GameConfig)[]
) {
  const port_sugestion = config.port ?? 8080;
  const game_name = config.name ?? "gameServer";
  const game_title = config.title ?? "This is a game server for a text game.";
  const game_version = config.version ?? "0.0.1";

  let argv = program
    .name(game_name)
    .description(game_title)
    .version(game_version)
    .option(
      "-p, --port <port>",
      "which port the server should listen to.",
      String(port_sugestion)
    )
    .parse();

  const opts = argv.opts();
  const port = Number(opts.port) ?? port_sugestion;

  const game_classes: { [key: string]: GameAndOptions } = {};
  const games: Game[] = [];
  const user_map: { [key: string]: User } = {};

  const game_list: string[] = [];

  /* Iterate over parameters to pair game with configuration */
  while (gameData.length > 0) {
    let game: GameConstructor;
    let option: GameOptions;

    const data = gameData.shift() ?? {};
    if (typeof data === "function") {
      game = data as GameConstructor;
    } else {
      // Should be change so a game class can have many configuration.
      console.error("A config file not pairing with a game.");
      continue;
    }

    let config: GameConfig;

    if (typeof gameData[0] === "object") {
      config = (gameData.shift() ?? {}) as GameOptions;
    } else {
      config = { name: game.name };
    }

    let options = Object.assign(config, {
      name: config.name,
      label: config.label ?? "",
      title: config.title ?? "",
    });

    game_classes[config.name] = {
      game: game,
      options: options,
    };
    game_list.push(config.name);
  }

  const app = express();

  app.use(logging);

  // To parse data from request
  app.use(express.urlencoded({ extended: true }));
  app.use(express.json());

  //  app.use(express.json());
  app.use(cookieParser());

  // Login a user
  app.use(connect(user_map));

  // Main response
  app.get("/", function (req, res) {
    const lang = "sv";
    let result = "";

    /* Give information about game */

    result += "# 🔥 Välkommen till att spela ett av våra spel! 🔥 \n";

    if (res.locals.user === undefined) {
      let user = new User("");
      res.locals.user = user;
      user_map[user.uuid] = user;
      res.cookie("token", user.uuid, { maxAge: 100000 });
    }

    if (config.simpel === true) {
      const options = game_classes[Object.keys(game_classes)[0]].options;
      const title = options.title ?? "";
      result += '**Nu ska vi spela "' + title + "**\n";
      result += `\n➡️/create/${options.name}\n\n`;
    } else if (res.locals.user.name === "") {
      result += "🎲 Det är bra om du har ett alias";
      result += "\n➡️/name\n\n";
    } else {
      result += "🎲 Hopppas du kommer tycka om våra spel!";
      result += "\n➡️/select\n\n";
    }

    res.send(result);
  });

  // Giva a name or rather an alias to user.
  app.get("/name", function (req, res) {
    const alias =
      typeof req.query["alias"] === "string" ? req.query["alias"] : undefined;
    name(req, res, alias);
  });

  app.post("/name", function (req, res) {
    const alias = req.body.alias;
    name(req, res, alias);
  });

  function name(req: express.Request, res: express.Response, name?: string) {
    let result = "";
    if (name === undefined) {
      result += "❓alias Vilket alias vill du använda?";
      result += "\n➡️/name\n\n";
    } else {
      res.locals.user.name = name;
      result += `🎲 Hopppas ${name} kommer tycka om våra spel!`;
      result += "\n➡️/select\n\n";
    }
    res.send(result);
  }

  // Let the user select a game

  app.get("/select", function (req, res) {
    const index =
      typeof req.query.index === "string" ? Number(req.query.index) : NaN;
    select(req, res, index);
  });

  app.post("/select", function (req, res) {
    const index = Number(req.body.index);
    select(req, res, index);
  });

  function select(
    req: express.Request,
    res: express.Response,
    gameIndex: number
  ) {
    let result = "";
    if (isNaN(gameIndex)) {
      result += "#🔥 Välj ett av våra spel! 🔥\n";
      let game_list_text = "";
      for (const game of game_list) {
        let options = game_classes[game].options;
        game_list_text += options.title;
      }
      result += game_list_text;
      result += "❓index Vilket spel vill du spela? ";
      result += "\n➡️/select\n\n";
    } else {
      const game = game_list[gameIndex - 1];
      let options = game_classes[game].options;
      result += `\🎲 Du har valt att spela: "${options.title}"`;
      result += "\n➡️/create/${game}\n\n";
    }
    res.send(result);
  }

  // Not implemented yet, for multiplayer games.
  app.get("/room", function (req, res) {
    res.send("Not implemented yet!");
  });

  // Not implemented yet, for multiplayer games.
  app.get("/join/:gameId", function (req, res) {
    res.send("Not implemented yet!");
  });

  // Create a game depending on selection
  app.get("/create/:game", function (req, res) {
    const game_and_options = game_classes[req.params.game];
    let result = "";

    if (game_and_options !== undefined) {
      const game: Game = new game_and_options.game(game_and_options.options);
      res.locals.user.gameId = games.length;
      games.push(game);
      if (game.type === "singel") {
        const player = new Player(res.locals.user.name, game.nextId);
        game.join(player);
        res.locals.user.player = player;
        result += "Spelet är klart, dax att spela\n";
        result += game.start();
        result += "\n➡️/next\n\n";
      }
    } else {
      throw new Error("No game with that name.");
    }
    res.send(result);
  });

  // Not implemented yet, for multiplayer games.
  app.get("/start/:game", function (req, res) {
    res.send("Not implemented yet!");
  });

  // Game loop management
  app.get("/next", function (req, res) {
    const answer: { [key: string]: number | string } = {};
    for (const key of Object.keys(req.query)) {
      const value = Number.isNaN(Number(req.query[key]))
        ? Number(req.query[key])
        : String(req.query[key]);
      answer[key] = value;
    }

    res.send(doNext(res.locals.user, answer, games));
  });

  app.post("/next", function (req, res) {
    res.send(doNext(res.locals.user, req.body, games));
  });

  function doNext(
    user: User,
    answer: { [key: string]: number | string },
    games: Game[]
  ): string {
    let result = "";
    if (
      user !== undefined &&
      user.player !== undefined &&
      user.gameId !== undefined
    ) {
      let game = games[user.gameId];
      let status = game.next(user.player, answer);
      result += user.player.msg;
      user.player.msg = "";
      if (status === "end") {
        result += "\n➡️/end\n\n";
      } else {
        result += "\n➡️/next\n\n";
      }
    } else {
      throw new Error("No player found.");
    }
    return result;
  }

  // End game
  app.get("/end", function (req, res) {
    let do_next: string;
    if (config.simpel === true) {
      let game_id = res.locals.user.gameId;
      let game = games[game_id];
      do_next = `/create/${game.name}\n\n`;
    } else {
      do_next = "/select\n\n";
    }
    let result = "Spelet är slut!" + "\n➡️do_next\n\n";
    res.send(result);
  });

  // Start the server.
  app.listen(port, () => {
    console.log("You can find this server on: http://localhost:" + port);
  });
}

function connect(userMap: { [key: string]: User }) {
  return function (req: express.Request, res: express.Response, next: any) {
    if (req.cookies !== undefined && req.cookies.token) {
      const user = userMap[req.cookies.token];
      res.locals.user = user;
    }
    next();
  };
}

function logging(
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) {
  const datetime = new Date();
  const hours = datetime.getHours();
  const minutes = datetime.getMinutes();
  const seconds = datetime.getSeconds();
  let msg = console.log(
    "[" +
      (Number(hours) > 9 ? hours : "0" + hours) +
      ":" +
      (Number(minutes) > 9 ? minutes : "0" + minutes) +
      ":" +
      (Number(seconds) > 9 ? seconds : "0" + seconds) +
      "]" +
      " #" +
      req.originalUrl +
      " @" +
      req.ip
  );
  next();
}
