import { v4 as uuid } from "uuid";
import { Player } from "./player";

export class User {
  static next = 0;
  id: number;
  name: string;
  gameId: number | undefined;
  player?: Player;
  uuid: string;

  constructor(name: string) {
    this.name = name;
    this.uuid = uuid();
    this.id = User.next;
    User.next++;
  }
}
