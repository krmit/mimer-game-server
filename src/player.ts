export class Player {
  name: string;
  msg: string;
  id: number;

  constructor(name: string, id: number) {
    this.name = name;
    this.msg = "";
    this.id = id;
  }
}
