import { Player } from "./player";

export interface GameConfig {
  max?: number;
  min?: number;
  length?: number;
  name: string;
  label?: string;
  title?: string;
  description?: string;
}

export interface GameOptions {
  max?: number;
  min?: number;
  length?: number;
  name?: string;
  label?: string;
  title?: string;
}

export type GameStatus = "wait" | "start" | "next" | "end";
export type GameType = "singel" | "turn" | "concurrently";

export abstract class Game {
  players: Player[] = [];
  status: GameStatus = "start";
  abstract type: GameType;
  name: string;
  label: string;
  static description: string;
  msg: string = "";

  constructor(config: GameConfig) {
    this.name = config.name ?? "";
    this.label = config.label ?? this.name; // Bug: Need to ramove spaces.
  }

  join(...players: Player[]) {
    this.players = this.players.concat(players);
    for (let i = 0; i < players.length; i++) {
      players[i].id = this.players.length + i;
    }
  }

  get nextId() {
    return this.players.length + 1;
  }

  abstract start(): string;

  abstract next(
    player: Player,
    answer: { [key: string]: number | string }
  ): GameStatus;
}
