import { server } from "../src";
import { TestOfText } from "./test-of-text";

const configServer = {
  port: 8080,
  simpel: true,
  name: "testOfTextServer",
};

const configGame = {
  name: "TestOfText",
  max: 10,
  min: 0,
  label: "testar några olika texter",
  title: "Test av Text",
};

server(configServer, TestOfText, configGame);
