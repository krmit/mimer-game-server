import { server } from "../src";
import { BinHex } from "./binhex-question";

const configServer = {
  port: 8080,
  simpel: true,
};

const configGame = {
  name: "binhex10",
  length: 10,
  label: "Binära till hex",
  title: "Binära till hexadecimala tal",
  translation: [
    {
      en: "Binary to hexadecimal numbers",
      sv: "Binära till hexadecimala tal",
    },
    {
      en: "Binary to hex",
      sv: "Binära till hex",
    },
  ],
};

server(configServer, BinHex, configGame);
