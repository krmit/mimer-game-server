import { Player, Game, GameConfig, GameStatus } from "../src";

export class TestOfText extends Game {
  static creators = ["krm"];
  static description: string = "";

  numberToGuess = 0;
  max: number;
  page = 0;
  texts: string[] = [];

  constructor(config: GameConfig) {
    super({ name: config.name });
    this.page = config.min ?? 1;
    this.max = config.max ?? 8;

    let text = "";
    text = "#The quick brown fox jumps over the lazy dog";
    this.texts.push(text);

    text = "**The quick brown fox jumps over the lazy dog**";
    this.texts.push(text);

    text = "🔥 The quick brown fox jumps over the lazy dog";
    this.texts.push(text);

    text = "```The quick brown fox jumps over the lazy dog```";
    this.texts.push(text);

    text = "Abba\nBccb\nCddc\nDeed\nEffe";
    this.texts.push(text);

    text = "- Abba\n- Bccb\n- Cddc\n- Deed\n- Effe";
    this.texts.push(text);

    text = "1. Abba\n2. Bccb\n3. Cddc\n4. Deed\n5. Effe";
    this.texts.push(text);

    text = "Abba\nBccb\n🔥 Cddc\nDeed\nEffe";
    this.texts.push(text);
  }

  start() {
    let result = "Visar några test fram till test nummer " + this.max;
    return result;
  }

  next(player: Player, answer: { guess: number | string }): GameStatus {
    let guess = answer.guess;
    player.msg += `# Test ${this.page}`;
    player.msg += this.texts[this.page - 1];
    this.page++;
    if (!(this.page - 1 < this.max)) {
      return "end";
    }
    return "next";
  }
}
