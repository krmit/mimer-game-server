#!/usr/bin/env bash
show() {
batcat -n -l md
}

next()
{
  curl -b cookiefile -s -c cookiefile -XPOST  -H "Content-Type: application/json" -d "{\"$1\":$2}" http://localhost:8080/next | show
}

start() {
curl -b cookiefile -s -c cookiefile http://localhost:8080/ | show;
curl -b cookiefile -s -c cookiefile http://localhost:8080/create/$1 | show;
curl -b cookiefile -s -c cookiefile http://localhost:8080/next | show;
}