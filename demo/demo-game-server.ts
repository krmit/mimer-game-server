#!/usr/bin/env node
import { server } from "../src";
import { TestOfText } from "./test-of-text";
import { GuessNumber } from "./guess-number";
import { BinHex } from "./binhex-question";

const configServer = {
  port: 8080,
  name: "gameServerDemo",
  title: "A demo of the game server.",
  version: "0.1.0",
};

const configBinHex = {
  name: "binhex1",
  length: 5,
  label: "binära till hex",
  title: "Binära till hexadecimala tal",
};

const configBinHex_2 = {
  name: "binhex2",
  length: 5,
  max: 2,
  label: "en byte till hex",
  title: "En byte till hexadecimala tal",
};

const configGuessNumber_10 = {
  name: "GuessNumber10",
  max: 10,
  min: 0,
  label: "spelet gissa ett tal mellan 1 och 10",
  title: "Gissa ett tal mellan 1 och 10",
};

const configGuessNumber_20 = {
  name: "GuessNumber20",
  max: 20,
  min: 0,
  label: "spelet gissa ett tal mellan 1 och 10",
  title: "Gissa ett tal mellan 1 och 10",
};

const configGuessNumber_100 = {
  name: "GuessNumber_100",
  max: 100,
  min: 0,
  label: "spelet gissa ett tal mellan 1 och 100",
  title: "Gissa ett tal mellan 1 och 100",
};

const configTestText = {
  name: "TestOfText",
  label: "testar några olika texter",
  title: "Test av Text",
};

server(
  configServer,
  GuessNumber,
  configGuessNumber_10,
  GuessNumber,
  configGuessNumber_20,
  GuessNumber,
  configGuessNumber_100,
  BinHex,
  configBinHex,
  BinHex,
  configBinHex_2,
  TestOfText,
  configTestText
);
