import { server } from "../src";
import { GuessNumber } from "./guess-number";

const configServer = {
  port: 8080,
  simpel: true,
};

const configGame = {
  name: "GuessNumber20",
  max: 20,
  min: 0,
  label: "spelet gissa ett tal mellan 1 och 20",
  title: "Gissa ett tal mellan 1 och 20",
};

server(configServer, GuessNumber, configGame);
