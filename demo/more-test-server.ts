import { server } from "../src";
import { TestOfText } from "./test-of-text";

const configServer = {
  port: 8080,
};

const configGame1 = {
  name: "TestOfText1",
  max: 5,
  min: 1,
  label: "testar några olika texter",
  title: "Test av Text",
  translation: [
    {
      en: "Test of Text",
      sv: "Test av Text",
    },
    {
      en: "some test of text",
      sv: "testar några olika texter",
    },
  ],
};

const configGame2 = Object.assign({}, configGame1);
configGame2.name = "TestOfText2";
configGame2.title = "Test av Text 2";
configGame2.min = 6;
configGame2.max = 8;

server(configServer, TestOfText, configGame1, TestOfText, configGame2);
