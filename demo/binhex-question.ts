import { Player, Game, GameConfig, GameStatus } from "../src";

export class BinHex extends Game {
  static creators = ["krm"];
  static description: string = "";
  answer = "";
  letters: number;
  points = 0;
  turns = 0;
  length: number;

  constructor(config: GameConfig) {
    super({ name: config.name ?? "Översätt från binärt till hexadecimalt." });
    this.letters = config.max ?? 1;
    this.length = config.length ?? 5;
  }

  start() {
    let result = "";
    result += "Skriv vad det binära tal blir i hexadecimalt";
    result += "Svara med små bokstäver\n";
    this.doQuestion(result);
    this.points = 0;
    this.turns = 0;
    return result;
  }

  next(player: Player, answer: { answer: string }): GameStatus {
    if (answer.answer !== this.answer) {
      player.msg += `🙁 ${answer.answer} != ${this.answer}`;
    } else {
      this.points++;
    }
    this.turns++;
    if (this.turns < this.length) {
      this.doQuestion(player.msg);
    } else {
      player.msg += "Du hade ";
      player.msg += String(this.points);
      player.msg += " rätt av " + this.length + ".";
      return "end";
    }
    return "next";
  }

  private doQuestion(result: string) {
    const solution = Math.floor(Math.random() * 16 ** this.letters);
    const binary_text = printBinary(solution, this.letters);
    result += "\n❓answer" + binary_text + "?";
    this.answer = printHex(solution, this.letters);
    return binary_text;
  }
}

function printBinary(bin: number, nibbles: number) {
  let filter = 1;
  let result = "";
  while (bin > filter - 1) {
    if ((bin & filter) === 0) {
      result = "0" + result;
    } else {
      result = "1" + result;
    }
    filter = filter << 1;
  }
  result = "0".repeat(nibbles * 4 - result.length) + result;

  return result;
}

function printHex(hex: number, nibbles: number) {
  let result = "";
  const alphabet = [
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
  ];

  for (let i = 0; i < nibbles; i++) {
    let nibbel = Math.floor((hex / 16 ** i) % 16);
    result = alphabet[nibbel] + result;
  }

  return result;
}
