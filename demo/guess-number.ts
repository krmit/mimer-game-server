import { Player, Game, GameConfig, GameType, GameStatus, server } from "../src";

export class GuessNumber extends Game {
  static creators = ["krm"];
  static description: string = "Ett spel där du ska gissa ett tal.";
  type: GameType = "singel";

  numberToGuess = 0;
  maxNumber: number;
  minNumber: number;
  guesses = 0;

  constructor(config: GameConfig) {
    super({ name: config.name ?? "Gissa ett numer." });
    this.maxNumber = config.max ?? 10;
    this.minNumber = config.min ?? 0;
  }

  start() {
    this.numberToGuess =
      Math.floor(Math.random() * this.maxNumber) + this.minNumber;
    let result = "";
    result += `Gissa ett tal mellan ${this.minNumber} och ${this.maxNumber}\n`;
    result += "❓guess Gissa: ";
    return result;
  }

  next(player: Player, answer: { guess: number | string }): GameStatus {
    let guess = Number(answer.guess);
    this.guesses++;
    if (Number.isNaN(guess)) {
      player.msg += "Inte ett tal!\n";
    } else {
      if (guess < this.numberToGuess) {
        player.msg += "För lågt!\n";
      } else if (guess > this.numberToGuess) {
        player.msg += "För högt!\n";
      } else {
        player.msg += "Du har gissat rätt! \n";
        player.msg += "Det tog dig: " + this.guesses + " gissningar";
        return "end";
      }
    }
    player.msg += "❓guess Gissa igen: ";
    return "next";
  }
}
