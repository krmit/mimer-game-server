# Mimer game server

## Install

To use it in your own project

```sh
npm set registry http://htsit.se:4873
npm install @mimer/game-server
```

For testing a server.

```sh
git clone mimer-game-server
cd mimer-game-server
npm set registry http://htsit.se:4873
npm run build
```

## Usage

There are some demo servers in the demo folder.

```sh
node dist/demo/demo-game-server.js
```

To use it in your own project. Import it first.

```ts
import { server } from "../src";
```

Then call it with a game class and a game configuration.

```ts
server({ port: 8080, simpel: true }, MyGame, configGame);
```

## License

GPL version 3, see the file COPYING.

## Contribute

Please feel free to open an issue or submit a pull request.

## Changelog

- **0.1.0** _2022-07-08_ First version published to a private registry.

## Author

**©Magnus Kronnäs 2022 [magnus.kronnas.se](http://magnus.kronnas.se)**
